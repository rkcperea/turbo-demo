# Turbo-Demo
This is a demo, using TDD development as well. 

## Inventory app
- [ ] User Interface (UI)
  - [ ] Layout
  - [ ] Grid System
- [ ] Devise
  - [X] ~~*bundle add devise*~~ [2023-01-23]
  - [X] ~~*root to dashboard*~~ [2023-01-23]
  - [X] ~~*logout button*~~ [2023-01-23]
  - [ ] flash messages
- [ ] Dashboard
- [ ] System Test
- [ ] old CRUD
  - [X] ~~*Suppliers, Raw Materials*~~ [2023-01-24]
- [ ] a stock_keeping_unit CRUD
  - [ ] Models
  - [ ] Controllers
  - [ ] Views
- [ ] login
- [ ] notifications
- [ ] [Use](https://www.bootrails.com/blog/rails-and-sveltejs-tutorial/) Svelte 

# Sources
[Hotrails tutorial](https://www.hotrails.dev/turbo-rails) 
