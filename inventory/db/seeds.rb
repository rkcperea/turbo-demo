# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
# Create Superuser Admin #TODO, below fails
# user = User.create(email: 'rkcperea@gmail.com', password: 'admin') if User.count == 0
# puts "Created Initial User with email: #{user.email} and password: admin" if user
# puts user.save