class CreateRawMaterials < ActiveRecord::Migration[7.0]
  def change
    create_table :raw_materials do |t|
      t.references :supplier, null: false, foreign_key: true
      t.string :name
      t.date :expires_on
      t.string :unit
      t.json :remarks

      t.timestamps
    end
  end
end
