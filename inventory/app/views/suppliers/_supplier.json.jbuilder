json.extract! supplier, :id, :name, :contact, :remarks, :created_at, :updated_at
json.url supplier_url(supplier, format: :json)
