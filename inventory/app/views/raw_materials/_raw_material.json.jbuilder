json.extract! raw_material, :id, :supplier_id, :name, :expires_on, :unit, :remarks, :created_at, :updated_at
json.url raw_material_url(raw_material, format: :json)
