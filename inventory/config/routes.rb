Rails.application.routes.draw do
  resources :raw_materials
  resources :suppliers
  devise_for :users
  get 'dashboard/index'
  
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Devise gem
  # 2. Ensure you have defined root_url to *something* in your config/routes.rb.
  # Defines the root path route ("/")
  # root "articles#index"
  root "dashboard#index"

end
